import './land.css';

import Lighthouse from '../lighthouse/lighthouse';

function Land() {
  return (
    <div className="land">
      <Lighthouse />
    </div>
  );
}

export default Land;
