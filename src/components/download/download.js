import * as htmlToImage from 'html-to-image';
import { saveAs } from 'file-saver';

import './download.css';

function Download() {
  return (
    <div className="download">
      <button onClick={downloadImg} title="Download">Download</button>
    </div>
  );
}

function downloadImg() {
  const canvas = document.querySelector('.canvas');
  htmlToImage.toBlob(canvas)
  .then(function (blob) {
    saveAs(blob, 'lighthouse.png');
  });
}

export default Download;
