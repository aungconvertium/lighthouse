import './logo.css';
import logoImage from './C@20logo.png';

function Logo() {
  const logoSize = 120;

  return (
    <div className="logo">
      <img src={logoImage} alt="Convertium" width={logoSize} height={logoSize} />
    </div>
  );
}

export default Logo;
