import { useState } from 'react';
import './settings.css';

function Settings() {

  let [settingsDialogShown, setsettingsDialogShown] = useState('');
  // let [showSun, setShowSun] = useState(true);

  return (
    <div className="settings">
      <div className={'settings-dialog ' + settingsDialogShown}>
        <div className="settings-dialog-inner">
          <div className="settings-form">

            {/* <div className="form-row">
              <div className="form-col">
                <label htmlFor="showSun">Show Sun</label>
                <input type="checkbox" id="showSun" name="showSun"
                checked={showSun} onChange={e => setShowSun(showSun = !showSun)}/>
              </div>
            </div> */}

            <div className="form-row">
              <div className="form-col">
                <label>Sun color top</label>
                <input type="color" name="sunColorTop" id="sunColorTop"
                  defaultValue="#fa8607"
                  onChange={e => onChangeHandler('--sun-color-top', e.target.value)} />
              </div>
              <div className="form-col">
                <label>Sun color bottom</label>
                <input type="color" name="sunColorBottom" id="sunColorBottom"
                  defaultValue="#fff57b"
                  onChange={e => onChangeHandler('--sun-color-bottom', e.target.value)} />
              </div>
            </div>

            <div className="form-row">
              <div className="form-col">
                <label>Sun position left</label>
                <input type="number" min="0" max="100" name="sunPositionX" id="sunPositionX"
                  defaultValue="15"
                  onChange={e => onChangeHandler('--sun-position-x', e.target.value + '%')} />
              </div>
              <div className="form-col">
                <label>Sun position top</label>
                <input type="number" min="0" max="100" name="sunPositionY" id="sunPositionY"
                  defaultValue="35"
                  onChange={e => onChangeHandler('--sun-position-y', e.target.value + '%')} />
              </div>
            </div>

            <div className="form-row">
              <div className="form-col">
                <label>Sky color start</label>
                <input type="color" name="skyColorStart" id="skyColorStart"
                  defaultValue="#87ceeb"
                  onChange={e => onChangeHandler('--sky-color-start', e.target.value)} />
              </div>
              <div className="form-col">
                <label>Sky color end</label>
                <input type="color" name="skyColorEnd" id="skyColorEnd"
                  defaultValue="#47b9e6"
                  onChange={e => onChangeHandler('--sky-color-end', e.target.value)} />
              </div>
            </div>

            <div className="form-row">
              <div className="form-col">
                <label>Cloud color</label>
                <input type="color" name="cloudColor" id="cloudColor"
                  defaultValue="#f0f0f0"
                  onChange={e => onChangeHandler('--cloud-color', e.target.value)} />
              </div>
              <div className="form-col">
                <label>Cloud move</label>
                <input type="checkbox" name="cloudMove" id="cloudMove"
                  defaultChecked={true} data-true="running" data-false="paused"
                  onChange={e => onCheckHandler('--cloud-move', e)} />
              </div>
            </div>

            <div className="form-row">
              <div className="form-col">
                <label>Sea color top</label>
                <input type="color" name="seaColorTop" id="seaColorTop"
                  defaultValue="#3aabd1"
                  onChange={e => onChangeHandler('--ocean-color-top', e.target.value)} />
              </div>
              <div className="form-col">
                <label>Sea color bottom</label>
                <input type="color" name="seaColorBottom" id="seaColorBottom"
                  defaultValue="#002a65"
                  onChange={e => onChangeHandler('--ocean-color-bottom', e.target.value)} />
              </div>
            </div>

            <div className="form-row">
              <div className="form-col">
                <label>Land color</label>
                <input type="color" name="landColor" id="landColor"
                  defaultValue="#749454"
                  onChange={e => onChangeHandler('--land-color', e.target.value)} />
              </div>
            </div>

            <div className="form-row">
              <div className="form-col">
                <label>Lighthouse body color</label>
                <input type="color" name="lighthouseBodyColor" id="lighthouseBodyColor"
                  defaultValue="#fafafa"
                  onChange={e => onChangeHandler('--lighthouse-body-color', e.target.value)} />
              </div>
              <div className="form-col">
                <label>Lighthouse roof color</label>
                <input type="color" name="lighthouseRoofColor" id="lighthouseRoofColor"
                  defaultValue="#f0f0f0"
                  onChange={e => onChangeHandler('--lighthouse-roof-color', e.target.value)} />
              </div>
              <div className="form-col">
                <label>Lighthouse lantern color</label>
                <input type="color" name="lighthouseLanternColor" id="lighthouseLanternColor"
                  defaultValue="#ffffff"
                  onChange={e => onChangeHandler('--lighthouse-lantern-color', e.target.value)} />
              </div>
              <div className="form-col">
                <label>Lighthouse window color</label>
                <input type="color" name="lighthouseWindowColor" id="lighthouseWindowColor"
                  defaultValue="#aaaaaa"
                  onChange={e => onChangeHandler('--lighthouse-window-color', e.target.value)} />
              </div>
            </div>

            <div className="form-row">
              <div className="form-col">
                <label>Logo position left</label>
                <input type="number" min="0" max="100" name="logoPositionX" id="logoPositionX"
                  defaultValue="0"
                  onChange={e => onChangeHandler('--logo-position-x', e.target.value + '%')} />
              </div>
              <div className="form-col">
                <label>Logo position top</label>
                <input type="number" min="0" max="100" name="logoPositionY" id="logoPositionY"
                  defaultValue="0"
                  onChange={e => onChangeHandler('--logo-position-y', e.target.value + '%')} />
              </div>
              <div className="form-col">
                <label>Logo size</label>
                <input type="number" min="0" max="999" name="logoSize" id="logoSize"
                  defaultValue="120"
                  onChange={e => onChangeHandler('--logo-size', e.target.value + 'px')} />
              </div>
            </div>

          </div>
        </div>
        <div className="settings-close">
          <button onClick={e => setsettingsDialogShown(settingsDialogShown = '')}
          title="Close">Close</button>
        </div>
      </div>
      <button className="settings-open" title="Settings"
        onClick={e => setsettingsDialogShown(settingsDialogShown = 'shown')}
      >Settings</button>
    </div>
  );
}

function onCheckHandler(prop, e) {
  const trueval = e.target.dataset.true;
  const falseval = e.target.dataset.false;
  const checked = e.target.checked;

  if (checked) {
    document.documentElement.style.setProperty(prop, trueval);
  } else {
    document.documentElement.style.setProperty(prop, falseval);
  }
}

function onChangeHandler(prop, val) {
  document.documentElement.style.setProperty(prop, val);
}

export default Settings;
