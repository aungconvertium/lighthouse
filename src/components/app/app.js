import './app.css';

import Ocean from '../ocean/ocean';
import Sun from '../sun/sun';
import Cloud from '../cloud/cloud';
import Land from '../land/land';
import Download from '../download/download';
import Settings from '../settings/settings';
import Logo from '../logo/logo';

function App() {
  return (
    <main className="app">
      <div className="canvas">
        <Sun />
        <Cloud position="bottom" />
        <Cloud size="big" position="top" />
        <Cloud size="small" />
        <Ocean />
        <Land />
        <Logo />
      </div>
      <Settings />
      <Download />
    </main>
  );
}

export default App;
